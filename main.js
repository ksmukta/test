var bodyParser = require('body-parser');
var express = require ('express');
var mongoose = require ('mongoose');
var sessions = require ('client-sessions');

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var User = mongoose.model('User',new Schema({
	id:           ObjectId,
	firstName:    String,
	lastName:     String,
	email:        {type:String, unique:true},
	password:     String,
	hostel: 	  String,
	type:         String,
}));

var Complaint = mongoose.model('Complaint',new Schema({
	id:           ObjectId,
	category:      String,
	description:   String,
	submited_by:   String,
	affecting:     String,
	resolved: 	   Boolean,
	viewed: 	   Boolean, 
}));


var app =  express();
app.set('view engine', 'ejs');

mongoose.connect('mongodb://localhost/testserver2')

app.use(bodyParser.urlencoded({ extended: true }));

app.use(sessions({
    cookieName: 'session',
    secret: 'cop290_complaint_app',
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
}));

app.get('/login',function(req,res) {
	res.render('login');
});

app.get('/register',function(req,res) {
	res.render('register',{error: ""});
});

app.get('/submit_complaint',function(req,res) {
	if(req.session && req.session.user){
		res.render('submit_complaint');
	}else{
		res.redirect('/login');
	}
});

app.get('/dashboard',function(req,res) {
	if(req.session && req.session.user){
		res.render('dashboard',{user: req.session.user});
	}else{
		res.redirect('/login');
	}
});

app.get('/show_all_complaints',function(req,res) {
	if(req.session && req.session.user){
		Complaint.find(function(err,complaints){
			res.setHeader('Content-Type', 'application/json');
			res.send(complaints);
		});
	}else{
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify({ success: false, data: "session Expired" }));
	}
});

app.get('/show_individual_complaints',function(req,res) {
	if(req.session && req.session.user){
		Complaint.find({$and: [{submited_by:req.session.user.email},{category:"Individual"}]},function(err,complaints){
			res.setHeader('Content-Type', 'application/json');
			res.send(complaints);
		});
	}else{
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify({ success: false, data: "Session Expired" }));
	}
});

app.get('/show_hostel_complaints',function(req,res) {
	if(req.session && req.session.user){
		Complaint.find({$and: [{affecting:req.session.user.hostel},{category:"Hostel"}]},function(err,complaints){
			res.setHeader('Content-Type', 'application/json');
			res.send(complaints);
		});
	}else{
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify({ success: false, data: "Session Expired" }));
	}
});

app.get('/show_all_hostel_complaints',function(req,res) {
	if(req.session && req.session.user){
		Complaint.find({$and: [{category:"Hostel"}]},function(err,complaints){
			res.setHeader('Content-Type', 'application/json');
			res.send(complaints);
		});
	}else{
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify({ success: false, data: "Session Expired" }));
	}
});

app.get('/show_all_individual_complaints',function(req,res) {
	if(req.session && req.session.user){
		Complaint.find({$and: [{category:"Individual"}]},function(err,complaints){
			res.setHeader('Content-Type', 'application/json');
			res.send(complaints);
		});
	}else{
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify({ success: false, data: "Session Expired" }));
	}
});

app.get('/show_Institute_complaints',function(req,res) {
	if(req.session && req.session.user){
		Complaint.find({category:"Institute"},function(err,complaints){
			res.setHeader('Content-Type', 'application/json');
			res.send(complaints);
		});
	}else{
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify({ success: false, data: "Session Expired" }));
	}
});


app.post('/register',function(req,res) {
	var user = new User({
	  firstName:  req.body.firstName,
	  lastName:   req.body.lastName,
	  email:      req.body.email,
	  password:   req.body.password,
	  type:       req.body.type,
	  hostel: 	  req.body.hostel,
	});
	user.save(function(err) {
	  if (err) {
	    var error = 'Something bad happened! Please try again.';

	    if (err.code === 11000) {
	      error = 'That email is already taken, please try another.';
	    }

	    res.render('register', { error: error });
	  } else {
	    res.render('register', { error: "User Registered Successfully" });
	  }
	});
});

app.post('/login',function(req,res) {
	User.findOne({ email: req.body.email }, function(err, user) {
		//console.log(user._id); // getting id
	  var success,udata;
	  res.setHeader('Content-Type', 'application/json');
	  if (!user) {
	    success = false;
	    udata = "Incorrect Email/Password!";
	    res.send(JSON.stringify({ success: success, data: udata }));
	  } else {
	    if (req.body.password == user.password) {
	      req.session.user = user;
	      success = true;
	      res.send(JSON.stringify({ success: success, data: {
	        firstName: user.firstName,
	        lastName: user.lastName,
	        type: user.type,
	        email: user.email,
	        firstName: user.firstName,
	        hostel: user.hostel
	      } }));
	    } else {
	      success = false;
	      udata = "Incorrect Email/Password!";
	      res.send(JSON.stringify({ success: success, data: udata }));
	    }
	  }
	});
});


app.post('/submit_complaint',function(req,res) {
	if(req.session && req.session.user){
		var complaint = new Complaint({
		  category:   req.body.category,	//hostel,individual,insti
		  description:req.body.description,
		  affecting:  req.body.affecting,	//hostelname, individual name
		  submited_by:   req.session.user.email,
		  resolved: 	false,
		  viewed: 		false,
		});
		complaint.save(function(err) {
		  if (err) {
		    res.setHeader('Content-Type', 'application/json');
			res.send(JSON.stringify({ success: false, data: "Unexpected Error" }));
		  } else {
		    res.setHeader('Content-Type', 'application/json');
			res.send(JSON.stringify({ success: true, data: "Successfully Posted" }));
		  }
		});
	}else{
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify({ success: false, data: "Session Expired" }));
		console.log("error sign in first");
	}
});

app.listen(3000);